FROM php:7.4-cli

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y git
RUN php composer.phar install --no-scripts --no-plugins --no-interaction