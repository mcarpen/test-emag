<?php

namespace Tests\Services;

use App\Models\Players\Orderus;
use App\Models\Players\Player;
use App\Models\Players\WildBeast;
use App\Services\GameRound;
use App\Services\Roulette;
use PHPUnit\Framework\TestCase;

class GameRoundTest extends TestCase
{
    /** @test */
    public function it_applies_no_damage_if_hit_is_missed()
    {
        $player1 = new Orderus();
        $player2 = new WildBeast();

        $initialPlayerTwoHealth = $player2->getHealth();

        $roulette = $this->createMock(Roulette::class);
        $roulette->method('isWin')->willReturn(true);

        $gameRound = new GameRound($roulette);
        $roundReport = $gameRound->run($player1, $player2);

        $this->assertEquals('Attacker for this round: Orderus.', $roundReport->getMessages()[0]);
        $this->assertEquals('Orderus missed.', $roundReport->getMessages()[1]);
        $this->assertEquals('Total round damage: 0.', $roundReport->getMessages()[2]);
        $this->assertEquals($initialPlayerTwoHealth, $player2->getHealth());
    }

    /** @test */
    public function it_applies_normal_damage_if_no_skills_are_used()
    {
        $orderusStrength = 71;
        $beastDefence = 56;
        $expectedDamage = 15;

        $player1 = $this->mockPlayer(Orderus::class, 100, $orderusStrength);
        $player2 = $this->mockPlayer(WildBeast::class, 80, 1, $beastDefence);

        $initialPlayerTwoHealth = $player2->getHealth();

        $roulette = $this->createMock(Roulette::class);
        $roulette->method('isWin')->willReturn(false); // return false for miss calls and skill odds

        $gameRound = new GameRound($roulette);
        $roundReport = $gameRound->run($player1, $player2);

        $this->assertEquals('Attacker for this round: Orderus.', $roundReport->getMessages()[0]);
        $this->assertEquals("Orderus hits for {$expectedDamage} damage.", $roundReport->getMessages()[1]);
        $this->assertEquals("Total round damage: {$expectedDamage}.", $roundReport->getMessages()[2]);
        $this->assertEquals($initialPlayerTwoHealth - $expectedDamage, $player2->getHealth());
    }

    /** @test */
    public function it_applies_correctly_applies_offensive_skills()
    {
        $orderusStrength = 71;
        $beastDefence = 56;
        $expectedDamage = 15;
        $expectedDamageAfterSkill = 30;
        $initialDefenderHealth = 100;
        $finalDefenderHealth = 70;

        $player1 = $this->mockPlayer(Orderus::class, 90, $orderusStrength, 1);
        $player2 = $this->mockPlayer(WildBeast::class, $initialDefenderHealth, 1, $beastDefence);

        $roulette = $this->createMock(Roulette::class);
        $roulette->method('isWin')
            ->willReturnOnConsecutiveCalls(false, true); // return true for RapidStrike only

        $gameRound = new GameRound($roulette);
        $roundReport = $gameRound->run($player1, $player2);

        $this->assertEquals('Attacker for this round: Orderus.', $roundReport->getMessages()[0]);
        $this->assertEquals("Orderus hits for {$expectedDamage} damage.", $roundReport->getMessages()[1]);
        $this->assertEquals("Orderus uses Rapid Strike.", $roundReport->getMessages()[2]);
        $this->assertEquals("Total round damage: {$expectedDamageAfterSkill}.", $roundReport->getMessages()[3]);
        $this->assertEquals("Defender health: {$finalDefenderHealth}.", $roundReport->getMessages()[4]);
        $this->assertEquals($finalDefenderHealth, $player2->getHealth());
    }

    /** @test */
    public function it_applies_correctly_applies_defensive_skills()
    {
        $beastStrength = 71;
        $orderusDefence = 56;
        $expectedDamage = 15;
        $expectedDamageAfterSkill = 7.5;
        $initialDefenderHealth = 100;
        $finalDefenderHealth = 92.5;

        $player1 = $this->mockPlayer(WildBeast::class, 90, $beastStrength);
        $player2 = $this->mockPlayer(Orderus::class, $initialDefenderHealth, 1, $orderusDefence);

        $roulette = $this->createMock(Roulette::class);
        $roulette->method('isWin')
            ->willReturnOnConsecutiveCalls(false, true); // return true for MagicShield only

        $gameRound = new GameRound($roulette);
        $roundReport = $gameRound->run($player1, $player2);

        $this->assertEquals('Attacker for this round: Wild Beast.', $roundReport->getMessages()[0]);
        $this->assertEquals("Wild Beast hits for {$expectedDamage} damage.", $roundReport->getMessages()[1]);
        $this->assertEquals("Orderus uses Magic Shield.", $roundReport->getMessages()[2]);
        $this->assertEquals("Total round damage: {$expectedDamageAfterSkill}.", $roundReport->getMessages()[3]);
        $this->assertEquals("Defender health: {$finalDefenderHealth}.", $roundReport->getMessages()[4]);
        $this->assertEquals($finalDefenderHealth, $player2->getHealth());
    }

    private function mockPlayer(string $class, int $health = 100, int $strength = 80, int $defence = 50): Player
    {
        $roulette1 = $this->createMock(Roulette::class);
        $roulette1->method('randomFromRange')
            ->willReturnOnConsecutiveCalls($health, $strength, $defence, 1, 1);

        return new $class($roulette1);
    }
}