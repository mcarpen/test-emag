<?php

namespace Tests\Services;

use App\Models\Players\Player;
use App\Models\Players\Orderus;
use App\Models\Players\PlayerCollection;
use App\Models\Players\WildBeast;
use App\Models\RoundReport;
use App\Services\Game;
use App\Services\GameRound;
use App\Services\Roulette;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    /**
     * @test
     * @dataProvider playerOrderDataProvider
     */
    public function it_correctly_decides_who_goes_first(array $orderus, array $beast, string $first)
    {
        $player1 = $this->mockPlayer(Orderus::class, 0, $orderus['speed'], $orderus['luck']);
        $player2 = $this->mockPlayer(WildBeast::class, 0, $beast['speed'], $beast['luck']);

        $game = new Game(new GameRound());
        $roundReports = $game->run(new PlayerCollection($player1, $player2));

        $this->assertEquals(
            "Orderus initialized. Stats: Health: 0, Strength: 1, Defence: 1, Speed: {$orderus['speed']}, Luck: {$orderus['luck']}%.",
            $roundReports[0]->getMessages()[0]
        );
        $this->assertEquals(
            "Wild Beast initialized. Stats: Health: 0, Strength: 1, Defence: 1, Speed: {$beast['speed']}, Luck: {$beast['luck']}%.",
            $roundReports[0]->getMessages()[1]
        );
        $this->assertEquals("{$first} will go first.", $roundReports[0]->getMessages()[2]);
    }

    /** @test */
    public function it_throws_exception_if_speed_and_luck_are_equal()
    {
        $speed = 40;
        $luck = 25;

        $player1 = $this->mockPlayer(Orderus::class, 100, $speed, $luck);
        $player2 = $this->mockPlayer(WildBeast::class, 100, $speed, $luck);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Could not decide the first player. They have equal speed and luck.');

        $game = new Game(new GameRound());
        $game->run(new PlayerCollection($player1, $player2));
    }

    /** @test */
    public function it_displays_the_winner_in_the_last_round_report()
    {
        $beastHealth = 10;

        $player1 = $this->mockPlayer(Orderus::class, 100, 40);
        $player2 = $this->mockPlayer(WildBeast::class, $beastHealth, 30);

        $gameRound = $this->createMock(GameRound::class);
        $gameRound->method('run')
            ->willReturnCallback(function (Player $player1, Player $player2) use ($beastHealth) {
                $player2->applyDamage($beastHealth); // kill beast

                return new RoundReport();
            });

        $game = new Game($gameRound);
        $roundReports = $game->run(new PlayerCollection($player1, $player2));

        $this->assertEquals('Orderus won.', $roundReports[count($roundReports) - 1]->getMessages()[0]);
    }

    /** @test */
    public function it_stops_game_after_a_maximum_of_20_turns()
    {
        $player1 = $this->mockPlayer(Orderus::class, 100, 40);
        $player2 = $this->mockPlayer(WildBeast::class, 100, 39);

        $gameRound = $this->createMock(GameRound::class);
        $gameRound->method('run')
            ->willReturnCallback(function (Player $player1, Player $player2) {
                return new RoundReport(); // do nothing each round
            });

        $game = new Game($gameRound);
        $roundReports = $game->run(new PlayerCollection($player1, $player2));

        $this->assertEquals(42, count($roundReports)); // game init (1) + 20 turns each (40) + game summary (1)
        $this->assertEquals('No winner after 20 turns.', $roundReports[41]->getMessages()[0]);
    }

    public function playerOrderDataProvider(): array
    {
        return [
            [
                'orderus' => [
                    'speed' => 50,
                    'luck' => 30
                ],
                'beast' => [
                    'speed' => 51,
                    'luck' => 40
                ],
                'first' => 'Wild Beast'
            ],
            [
                'orderus' => [
                    'speed' => 48,
                    'luck' => 30
                ],
                'beast' => [
                    'speed' => 41,
                    'luck' => 40
                ],
                'first' => 'Orderus'
            ],
            [
                'orderus' => [
                    'speed' => 40,
                    'luck' => 29
                ],
                'beast' => [
                    'speed' => 40,
                    'luck' => 25
                ],
                'first' => 'Orderus'
            ],
            [
                'orderus' => [
                    'speed' => 40,
                    'luck' => 29
                ],
                'beast' => [
                    'speed' => 40,
                    'luck' => 40
                ],
                'first' => 'Wild Beast'
            ],
        ];
    }

    private function mockPlayer(string $class, int $health = 100, int $speed = 10, int $luck = 10): Player
    {
        $roulette = $this->createMock(Roulette::class);
        $roulette->method('randomFromRange')
            ->willReturnOnConsecutiveCalls($health, 1, 1, $speed, $luck);

        return new $class($roulette);
    }
}