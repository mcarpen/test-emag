<?php

namespace Tests\Models\Players;

use App\Models\Players\WildBeast;
use PHPUnit\Framework\TestCase;

class WildBeastTest extends TestCase
{
    /** @test */
    public function it_has_correct_name()
    {
        $beast = new WildBeast();

        $this->assertEquals('Wild Beast', $beast->getName());
    }

    /** @test */
    public function it_has_correct_skills()
    {
        $beast = new WildBeast();

        $this->assertEquals([], $beast->getOffensiveSkills());
        $this->assertEquals([], $beast->getDefensiveSkills());
    }
}