<?php

namespace Tests\Models\Players;

use App\Models\Players\Orderus;
use App\Models\Skills\MagicShield;
use App\Models\Skills\RapidStrike;
use PHPUnit\Framework\TestCase;

class OrderusTest extends TestCase
{
    /** @test */
    public function it_has_correct_name()
    {
        $orderus = new Orderus();

        $this->assertEquals('Orderus', $orderus->getName());
    }

    /** @test */
    public function it_has_correct_skills()
    {
        $orderus = new Orderus();

        $this->assertEquals([new RapidStrike()], $orderus->getOffensiveSkills());
        $this->assertEquals([new MagicShield()], $orderus->getDefensiveSkills());
    }
}