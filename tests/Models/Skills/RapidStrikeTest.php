<?php

namespace Tests\Models\Skills;

use App\Models\Skills\RapidStrike;
use PHPUnit\Framework\TestCase;

class RapidStrikeTest extends TestCase
{
    /** @test */
    public function it_uses_correct_odds()
    {
        $rapidStrike = new RapidStrike();

        $this->assertEquals(10, $rapidStrike->getChance());
    }

    /** @test */
    public function it_applies_correct_amount_of_damage()
    {
        $rapidStrike = new RapidStrike();

        $this->assertEquals(64, $rapidStrike->use(32));
    }
}