<?php

namespace Tests\Models\Skills;

use App\Models\Skills\MagicShield;
use PHPUnit\Framework\TestCase;

class MagicShieldTest extends TestCase
{
    /** @test */
    public function it_uses_correct_odds()
    {
        $magicShield = new MagicShield();

        $this->assertEquals(20, $magicShield->getChance());
    }

    /** @test */
    public function it_reduces_damage_by_correct_amount()
    {
        $magicShield = new MagicShield();

        $this->assertEquals(16, $magicShield->use(32));
    }
}