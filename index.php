<?php

require_once('./vendor/autoload.php');

use App\Models\Players\Orderus;
use App\Models\Players\PlayerCollection;
use App\Models\Players\WildBeast;
use App\Services\Game;
use App\Services\GameRound;

$players = new PlayerCollection(
    new Orderus(),
    new WildBeast()
);

$game = new Game(
    new GameRound()
);

$roundReports = $game->run($players);

echo "\n";

foreach ($roundReports as $index => $roundReport) {
    switch($index) {
        case 0:
            echo "Initializing game:\n";
            break;

        case count($roundReports) - 1:
            echo "Game result:\n";
            break;

        default:
            echo "Round {$index}:\n";
    }

    foreach($roundReport->getMessages() as $message) {
        echo $message . "\n";
    }

    echo "\n";
}