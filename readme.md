# Hero Game

## Installation / Running

1. Clone this repository
2. `cd <game-root>`
3. If you have php >=7.4 installed:
   
   - `composer install` (without --no-dev, if you want to run tests)
   - `php index.php`
   
4. If you don't have php > 7.4:
   
   - make sure you have the Docker daemon running
   - `cd <game-root>`
   - `docker build -t <some-image-name> .` (this will run composer install in the container, so it may take a while)
   - `docker run <some-image-name> php index.php` to run the game
   - `docker run <some-image-name> vendor/bin/phpunit` to run the tests
   - delete the container and the image after you're done (or keep it forever, i don't care)