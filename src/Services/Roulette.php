<?php

namespace App\Services;

class Roulette
{
    public function isWin(int $winPercentage): bool
    {
        return rand(1, 100) < $winPercentage;
    }

    public function randomFromRange(int $min, int $max): int
    {
        return rand($min, $max);
    }
}