<?php

namespace App\Services;

use App\Models\Players\Player;
use App\Models\Players\PlayerCollection;
use App\Models\RoundReport;

class Game 
{
    private const MAX_TOTAL_TURNS = 20;

    private GameRound $gameRoundService;

    public function __construct(GameRound $gameRoundService)
    {
        $this->gameRoundService = $gameRoundService;
    }

    public function run(PlayerCollection $players): array
    {
        $roundReports = [];
        $turn = 1;

        $roundZeroReport = new RoundReport();
        $roundZeroReport->pushMessage("{$players->get(0)->getName()} initialized. Stats: {$this->getPlayerStatsText($players->get(0))}.");
        $roundZeroReport->pushMessage("{$players->get(1)->getName()} initialized. Stats: {$this->getPlayerStatsText($players->get(1))}.");

        $currentPlayer = $this->getFirstPlayer($players);
        $roundZeroReport->pushMessage("{$players->get($currentPlayer)->getName()} will go first.");

        $roundReports[] = $roundZeroReport;

        while($players->get(0)->getHealth() && $players->get(1)->getHealth() && $turn <= self::MAX_TOTAL_TURNS * 2) {
            $attackingPlayer = $players->get($currentPlayer);
            $defendingPlayer = $players->get(1 - $currentPlayer);

            $roundReports[] = $this->gameRoundService->run($attackingPlayer, $defendingPlayer);

            $currentPlayer = 1 - $currentPlayer;
            $turn++;
        }

        $roundReports[] = $this->makeGameSummaryReport($players);

        return $roundReports;
    }

    private function getFirstPlayer(PlayerCollection $players): int
    {
        if ($players->get(0)->getSpeed() > $players->get(1)->getSpeed()) {
            return 0;
        }

        if ($players->get(0)->getSpeed() < $players->get(1)->getSpeed()) {
            return 1;
        }

        if ($players->get(0)->getLuck() > $players->get(1)->getLuck()) {
            return 0;
        }

        if ($players->get(0)->getLuck() < $players->get(1)->getLuck()) {
            return 1;
        }

        throw new \Exception('Could not decide the first player. They have equal speed and luck.');
    }

    private function getPlayerStatsText(Player $player): string
    {
        $stats = [
            "Health: {$player->getHealth()}",
            "Strength: {$player->getStrength()}",
            "Defence: {$player->getDefence()}",
            "Speed: {$player->getSpeed()}",
            "Luck: {$player->getLuck()}%",
        ];

        return implode(', ', $stats);
    }

    private function makeGameSummaryReport(PlayerCollection $players): RoundReport
    {
        $gameSummaryReport = new RoundReport();

        if ($this->nobodyWon($players)) {
            $gameSummaryReport->pushMessage('No winner after ' . self::MAX_TOTAL_TURNS . ' turns.');
        } else {
            $gameSummaryReport->pushMessage("{$this->getWinner($players)->getName()} won.");
        }

        return $gameSummaryReport;
    }

    private function nobodyWon(PlayerCollection $players): bool
    {
        return $players->get(0)->getHealth() && $players->get(1)->getHealth();
    }

    private function getWinner(PlayerCollection $players): Player
    {
        return $players->get(0)->getHealth() ? $players->get(0) : $players->get(1);
    }
}