<?php

namespace App\Services;

use App\Models\Players\Player;
use App\Models\RoundReport;

class GameRound
{
    private Roulette $roulette;

    public function __construct(Roulette $roulette = null)
    {
        $this->roulette = $roulette ?? new Roulette();
    }

    public function run(Player $attackingPlayer, Player $defendingPlayer): RoundReport
    {
        $roundReport = new RoundReport();

        $roundReport->pushMessage("Attacker for this round: {$attackingPlayer->getName()}.");

        $isMiss = $this->roulette->isWin($defendingPlayer->getLuck());
        $damage = 0;

        if ($isMiss) {
            $roundReport->pushMessage("{$attackingPlayer->getName()} missed.");
        } else {
            $damage = $attackingPlayer->getStrength() - $defendingPlayer->getDefence();
            $roundReport->pushMessage("{$attackingPlayer->getName()} hits for {$damage} damage.");

            foreach ($attackingPlayer->getOffensiveSkills() as $skill) {
                if ($this->roulette->isWin($skill->getChance())) {
                    $damage = $skill->use($damage);
                    $roundReport->pushMessage("{$attackingPlayer->getName()} uses {$skill->getName()}.");
                }
            }

            foreach ($defendingPlayer->getDefensiveSkills() as $skill) {
                if ($this->roulette->isWin($skill->getChance())) {
                    $damage = $skill->use($damage);
                    $roundReport->pushMessage("{$defendingPlayer->getName()} uses {$skill->getName()}.");
                }
            }

            $defendingPlayer->applyDamage($damage);
        }

        $roundReport->pushMessage("Total round damage: {$damage}.");
        $roundReport->pushMessage("Defender health: {$defendingPlayer->getHealth()}.");

        return $roundReport;
    }
}