<?php

namespace App\Models\Skills\Base;

interface Skill
{
    public function getName(): string;
    public function use(float $damage): float;
    public function getChance(): int;
}