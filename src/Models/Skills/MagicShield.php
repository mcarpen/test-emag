<?php

namespace App\Models\Skills;

use App\Models\Skills\Base\DefensiveSkill;

class MagicShield implements DefensiveSkill
{
    private const CHANCE = 20;
    private const DAMAGE_REDUCTION = 0.5;

    public function getName(): string
    {
        return 'Magic Shield';
    }

    public function getChance(): int
    {
        return self::CHANCE;
    }

    public function use(float $damage): float
    {
        return $damage * self::DAMAGE_REDUCTION;
    }
}