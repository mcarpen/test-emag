<?php

namespace App\Models\Skills;

use App\Models\Skills\Base\OffensiveSkill;

class RapidStrike implements OffensiveSkill
{
    private const CHANCE = 10;
    private const DAMAGE_MULTIPLIER = 2;

    public function getName(): string
    {
        return 'Rapid Strike';
    }

    public function getChance(): int
    {
        return self::CHANCE;
    }

    public function use(float $damage): float
    {
        return $damage * self::DAMAGE_MULTIPLIER;
    }
}