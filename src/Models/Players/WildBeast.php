<?php

namespace App\Models\Players;

class WildBeast extends Player
{
    private const NAME = 'Wild Beast';

    protected const MIN_HEALTH = 60;
    protected const MAX_HEALTH = 90;
    protected const MIN_STRENGTH = 60;
    protected const MAX_STRENGTH = 90;
    protected const MIN_DEFENCE = 40;
    protected const MAX_DEFENCE = 60;
    protected const MIN_SPEED = 40;
    protected const MAX_SPEED = 60;
    protected const MIN_LUCK = 25;
    protected const MAX_LUCK = 40;

    public function getName(): string
    {
        return self::NAME;
    }
}