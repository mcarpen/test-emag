<?php

namespace App\Models\Players;

class PlayerCollection
{
    private array $players;

    public function __construct(Player $player1, Player $player2)
    {
        $this->players = [$player1, $player2];
    }

    public function get(int $index): Player
    {
        if (!in_array($index, [0, 1])) {
            throw new \Exception('Invalid player index.');
        }

        return $this->players[$index];
    }
}