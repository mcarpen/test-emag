<?php

namespace App\Models\Players;

use App\Models\Skills\Base\DefensiveSkill;
use App\Models\Skills\Base\OffensiveSkill;
use App\Services\Roulette;

abstract class Player
{
    protected const MIN_HEALTH = 0;
    protected const MAX_HEALTH = 0;
    protected const MIN_STRENGTH = 0;
    protected const MAX_STRENGTH = 0;
    protected const MIN_DEFENCE = 0;
    protected const MAX_DEFENCE = 0;
    protected const MIN_SPEED = 0;
    protected const MAX_SPEED = 0;
    protected const MIN_LUCK = 0;
    protected const MAX_LUCK = 0;

    protected float $health;
    protected int $strength;
    protected int $defence;
    protected int $speed;
    protected int $luck;

    /** Skill[] */
    protected array $skills = [];

    public function __construct(Roulette $roulette = null)
    {
        $roulette = $roulette ?? new Roulette();

        $this->health = $roulette->randomFromRange(static::MIN_HEALTH, static::MAX_HEALTH);
        $this->strength = $roulette->randomFromRange(static::MIN_STRENGTH, static::MAX_STRENGTH);
        $this->defence = $roulette->randomFromRange(static::MIN_DEFENCE, static::MAX_DEFENCE);
        $this->speed = $roulette->randomFromRange(static::MIN_SPEED, static::MAX_SPEED);
        $this->luck = $roulette->randomFromRange(static::MIN_LUCK, static::MAX_LUCK);
    }

    public abstract function getName(): string;

    public function getHealth(): float
    {
        return $this->health;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getDefence(): int
    {
        return $this->defence;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    /** @return OffensiveSkill[] */
    public function getOffensiveSkills(): array
    {
        return array_values(
            array_filter($this->skills, function ($skill) {
                return $skill instanceof OffensiveSkill;
            })
        );
    }

    /** @return DefensiveSkill[] */
    public function getDefensiveSkills(): array
    {
        return array_values(
            array_filter($this->skills, function ($skill) {
                return $skill instanceof DefensiveSkill;
            })
        );
    }

    public function applyDamage(float $damage): void
    {
        $this->health -= min($damage, $this->health);
    }
}