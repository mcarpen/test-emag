<?php

namespace App\Models\Players;

use App\Models\Skills\MagicShield;
use App\Models\Skills\RapidStrike;
use App\Services\Roulette;

class Orderus extends Player
{
    private const NAME = 'Orderus';

    protected const MIN_HEALTH = 70;
    protected const MAX_HEALTH = 100;
    protected const MIN_STRENGTH = 70;
    protected const MAX_STRENGTH = 80;
    protected const MIN_DEFENCE = 45;
    protected const MAX_DEFENCE = 55;
    protected const MIN_SPEED = 40;
    protected const MAX_SPEED = 50;
    protected const MIN_LUCK = 10;
    protected const MAX_LUCK = 30;

    protected array $skills = [];

    public function __construct(Roulette $roulette = null)
    {
        parent::__construct($roulette);

        $this->skills = [
            new RapidStrike(),
            new MagicShield()
        ];
    }

    public function getName(): string
    {
        return self::NAME;
    }
}