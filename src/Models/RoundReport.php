<?php

namespace App\Models;

class RoundReport
{
    /** @var string[] */
    private array $messages = [];

    public function pushMessage(string $message)
    {
        $this->messages[] = $message;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }
}